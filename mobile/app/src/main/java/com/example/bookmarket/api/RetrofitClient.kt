package com.example.bookmarket.api

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitClient {
    private const val BASE_URL = "http://192.168.0.17:8000/" // Replace with your API base URL

    val instance: BookApiService by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(MoshiProvider.instance))
            .build()

        retrofit.create(BookApiService::class.java)
    }
}
