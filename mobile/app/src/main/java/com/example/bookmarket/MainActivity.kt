package com.example.bookmarket

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.awaitResponse

import com.example.bookmarket.api.RetrofitClient
import com.example.bookmarket.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val bookApiService = RetrofitClient.instance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Find the RecyclerView in the layout
        val recyclerView: RecyclerView = findViewById(R.id.recyclerViewBooks)

        // Fetch books on a background thread using Kotlin Coroutines
        GlobalScope.launch(Dispatchers.Main) {
            val response = withContext(Dispatchers.IO) {
                bookApiService.getBooks().awaitResponse()
            }

            if (response.isSuccessful) {
                val books = response.body() ?: emptyList()

                // Log the response for testing purposes
                Log.d("MainActivity", "Response: $books")

                // Create an instance of the custom adapter
                val adapter = BookAdapter(books) { selectedBook ->
                    // Handle item click, e.g., navigate to detail page
                    navigateToDetailPage(selectedBook)
                }

                // Set the adapter and layout manager for the RecyclerView
                recyclerView.adapter = adapter
                recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
            } else {
                // Handle error
                Log.e("MainActivity", "Error: ${response.code()}")
            }
        }
    }

    private fun navigateToDetailPage(book: Book) {
        val intent = Intent(this, BookDetailsActivity::class.java)
        intent.putExtra("BOOK_TITLE", book.title)
        intent.putExtra("BOOK_AUTHOR", book.author)
        startActivity(intent)
    }
}
