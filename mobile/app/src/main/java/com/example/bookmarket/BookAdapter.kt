package com.example.bookmarket

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bookmarket.databinding.BookItemBinding

class BookAdapter(private val books: List<Book>, private val onItemClick: (Book) -> Unit) :
    RecyclerView.Adapter<BookAdapter.BookViewHolder>() {

    class BookViewHolder(private val binding: BookItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(book: Book) {
            // Use the book object to set data to UI elements using the binding object
            binding.bookTitleTextView.text = book.title
            binding.bookAuthorTextView.text = book.author
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        // Inflate the layout using the binding object
        val binding = BookItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BookViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val book = books[position]
        holder.bind(book)

        // Set OnClickListener for the item
        holder.itemView.setOnClickListener {
            onItemClick.invoke(book)
        }
    }

    override fun getItemCount(): Int {
        return books.size
    }
}
