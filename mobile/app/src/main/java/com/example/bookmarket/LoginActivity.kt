// LoginActivity.kt
package com.example.bookmarket

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.example.bookmarket.api.RetrofitClient
import com.example.bookmarket.data.models.LoginResponse
import com.example.bookmarket.databinding.ActivityLoginBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.awaitResponse

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val bookApiService = RetrofitClient.instance
    private val ACCESS_TOKEN_KEY = "access_token"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Set up click listener for the login button
        binding.buttonLoginLogout.setOnClickListener {
            val username = binding.editTextUsername.text.toString()
            val password = binding.editTextPassword.text.toString()

            // Call your API for user authentication
            GlobalScope.launch(Dispatchers.Main) {
                val response = withContext(Dispatchers.IO) {
                    bookApiService.login(username, password).awaitResponse()
                }

                if (response.isSuccessful) {
                    val loginResponse: LoginResponse? = response.body()

                    if (loginResponse != null) {
                        // If login is successful, store the access token in SharedPreferences
                        val accessToken = loginResponse.accessToken
                        saveAccessToken(accessToken)

                        // Start another activity or perform other actions
                        val intent = Intent(this@LoginActivity, StartPageActivity::class.java)
                        startActivity(intent)
                        finish() // Optional: finish the LoginActivity to prevent going back
                    } else {
                        // Handle unexpected response format
                        handleLoginFailure()
                    }
                } else {
                    // Handle unsuccessful login
                    handleLoginFailure()
                }
            }
        }
    }

    private fun saveAccessToken(accessToken: String) {
        // Save the access token in SharedPreferences
        val sharedPreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(ACCESS_TOKEN_KEY, accessToken)
        editor.apply()
    }

    private fun handleLoginFailure() {
        binding.textViewError.visibility = TextView.VISIBLE
        Snackbar.make(binding.buttonLoginLogout, "Login failed", Snackbar.LENGTH_SHORT).show()
    }
}
