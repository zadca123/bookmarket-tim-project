package com.example.bookmarket.data.models


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.bookmarket.Book
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
@Entity(tableName = "user_table")
@JsonClass(generateAdapter = true)
data class User(
    @Json(name = "held_books")
    val heldBooks: List<Book?>,

    @Json(name = "id")
    @PrimaryKey(autoGenerate = false)
    val id: Int,

    @Json(name = "is_active")
    val isActive: Boolean,
    @Json(name = "owned_books")
    val ownedBooks: List<Book?>,
    @Json(name = "username")
    val username: String
)