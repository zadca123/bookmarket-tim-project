// ./com/example/bookmarket/BookDetailsActivity.kt
package com.example.bookmarket

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.bookmarket.databinding.ActivityBookDetailsBinding

class BookDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBookDetailsBinding
    private var BOOK_TITLE = "BOOK_TITLE"
    private var BOOK_AUTHOR = "BOOK_AUTHOR"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBookDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.d("BOOK_TITLE", BOOK_TITLE)
        if (intent.hasExtra(BOOK_TITLE)) {

            binding.bookTitleTextView.text = intent.getStringExtra(BOOK_TITLE)
            binding.bookAuthorTextView.text = intent.getStringExtra(BOOK_AUTHOR)
        }
    }
}
