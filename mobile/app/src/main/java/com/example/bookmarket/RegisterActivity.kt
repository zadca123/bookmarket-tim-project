package com.example.bookmarket

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.bookmarket.api.BookApiService
import com.example.bookmarket.api.RetrofitClient
import com.example.bookmarket.data.models.User
import com.example.bookmarket.databinding.ActivityRegisterBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private val bookApiService = RetrofitClient.instance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonRegister.setOnClickListener {
            val username = binding.editTextUsername.text.toString()
            val password = binding.editTextPassword.text.toString()

            registerUser(username, password)
        }
    }

    private fun registerUser(username: String, password: String) {

        val request = bookApiService.register(username, password)
        Log.d("NetworkRequest", "request: ${request}")
        request.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful) {
                    val user: User? = response.body()
                    // Handle successful registration
                } else {
                    // Handle registration failure
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                // Handle network request failure
            }
        })
    }
}

