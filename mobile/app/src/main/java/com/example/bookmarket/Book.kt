package com.example.bookmarket

data class Book(
    val id: Int,
    val title: String,
    val author: String,
    val owner_id: Int,
    val holder_id: Int?,
)
