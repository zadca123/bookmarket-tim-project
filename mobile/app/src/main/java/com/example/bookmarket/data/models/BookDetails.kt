package com.example.bookmarket.data.models


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "books")
@JsonClass(generateAdapter = true)
data class BookDetails(
    @Json(name = "author")
    val author: String,
    @Json(name = "description")
    val description: String,
    @Json(name = "genre")
    val genre: String,

    @Json(name = "id")
    @PrimaryKey(autoGenerate = false)
    val id: Int,

    @Json(name = "is_available")
    val isAvailable: Boolean,
    @Json(name = "release_date")
    val releaseDate: String,
    @Json(name = "title")
    val title: String
)