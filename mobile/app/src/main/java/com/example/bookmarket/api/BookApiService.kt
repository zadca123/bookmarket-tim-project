package com.example.bookmarket.api

import retrofit2.Call
import retrofit2.http.GET
import com.example.bookmarket.Book
import com.example.bookmarket.data.models.LoginResponse
import com.example.bookmarket.data.models.User
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface BookApiService {
    @GET("book")
    fun getBooks(): Call<List<Book>>

    @FormUrlEncoded
    @POST("login/token")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<LoginResponse>

    @POST("book/{id}/borrow/")
    fun borrowOrReturnBook(
        @Path("id") bookId: Int,
        @Header("Authorization") userToken: String
    ): Call<Book>

//    @FormUrlEncoded
    @POST("user")
    fun register(
        @Field("username") username: String,
        @Field("password") password: String,
        // Add any other registration fields you need
    ): Call<User>
}
