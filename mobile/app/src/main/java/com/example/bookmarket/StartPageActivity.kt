package com.example.bookmarket

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.bookmarket.api.RetrofitClient
import com.example.bookmarket.databinding.ActivityStartPageBinding
import com.google.zxing.integration.android.IntentIntegrator
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StartPageActivity : AppCompatActivity() {
    //    private val HARDCODED_BOOK_ID = 5
    private lateinit var binding: ActivityStartPageBinding
    private val bookApiService = RetrofitClient.instance
    private val sharedPreferences by lazy {
        getSharedPreferences("MyPrefs", Context.MODE_PRIVATE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        updateQRButtonVisibility(isLoggedIn())
        updateLoginLogoutButtonText(isLoggedIn())

        binding.buttonLoginLogout.setOnClickListener {
            if (isLoggedIn()) {
                // Implement logout logic here
                sharedPreferences.edit().remove("access_token").apply()
                // Update UI or perform any other necessary actions for logout
                updateLoginLogoutButtonText(false)
                updateQRButtonVisibility(false)
            } else {
                // Launch LoginActivity when the "Login" button is clicked
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }

        binding.buttonViewBooks.setOnClickListener {
            // Navigate to the MainActivity (book list)
            startActivity(Intent(this, MainActivity::class.java))
        }

        binding.buttonScanQR.setOnClickListener {
            // Start QR code scanning
            initiateQRCodeScan()
        }
        binding.buttonRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }
    private fun updateLoginLogoutButtonText(isLoggedIn: Boolean) {
        val loginButton = binding.buttonLoginLogout
        loginButton.text = if (isLoggedIn) "Logout" else "Login"
    }

    private fun updateQRButtonVisibility(isLoggedIn: Boolean) {
        val qrButton = binding.buttonScanQR
        qrButton.visibility = if (isLoggedIn) View.VISIBLE else View.GONE
    }

    private fun initiateQRCodeScan() {
        IntentIntegrator(this).initiateScan()
//        handleQRCodeResult("{\"id\": $HARDCODED_BOOK_ID}")
    }

    // Override onActivityResult to handle the result of the QR code scan
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents != null) {
                // Handle the scanned QR code result
                handleQRCodeResult(result.contents)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleQRCodeResult(qrCodeResult: String) {
        try {
            // Parse the JSON content from the QR code result
            val jsonObject = JSONObject(qrCodeResult)
            val bookId = jsonObject.getInt("id")
            makeNetworkRequest(bookId)


        } catch (e: JSONException) {
            // Handle JSON parsing exception
            e.printStackTrace()
            // Show an error message or take appropriate action
        }
    }

    private fun makeNetworkRequest(bookId: Int) {
        val accessToken = sharedPreferences.getString("access_token", null)
        Log.d("NetworkRequest", "access_token: " + accessToken.toString())
        val request = if (accessToken != null) {
            bookApiService.borrowOrReturnBook(bookId, "Bearer $accessToken")
        } else {
            // Handle the case when the user is not logged in
            // You might want to show a message or navigate to the login screen
            return
        }

        request.enqueue(object : Callback<Book> {
            override fun onResponse(call: Call<Book>, response: Response<Book>) {
                // Handle the response
                Log.d("NetworkRequest", "Response: ${response}")
                Log.d("NetworkRequest", "Response Code: ${response.code()}")
                Log.d("NetworkRequest", "Response body: ${response.body()}")
                if (response.isSuccessful) {
                    // Request successful
                    // You can take further action if needed
                } else {
                    // Request failed
                    // Handle the error
                }
            }

            override fun onFailure(call: Call<Book>, t: Throwable) {
                Log.e("NetworkRequest", "Network request failed: ${t.message}", t)
            }
        })
    }

    private fun isLoggedIn(): Boolean {
        val accessToken = sharedPreferences.getString("access_token", null)
        return accessToken != null
    }
}
