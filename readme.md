# Bookmarket
Application provides book sharing system where users can borrow books to each other.
## Functionalities
1. Authentication and authorization
    - Sign up and sign in via app
    - Authorization is made through the JWT token
2. Not authenticated user can
	  - List book available in system
3. Authenticated user can
	  - Add new book to system and then delete/update this book
	  - Borrow and return the book
4. Mobile user can
	  - Borrow the book via QR code
## Model
1. Book
	  - id
	  - title
	  - author
	  - genreOfBook
	  - releaseOfBook
	  - description
	  - isAvailable
	  - owner --> User
	  - holder ?--> User
	
2. User
	  - username
	  - password 
## Tech stack
1. Database - PostgresSQL
2. Backend - fastAPI (python)
3. Frontend - angular
4. Mobile - React Native
