from datetime import datetime

from pydantic import BaseModel


class BookShow(BaseModel):
    id: int
    title: str
    author: str
    owner_id: int
    holder_id: int | None

    # makes pydantic converting non dict objects to json
    class Config:
        from_attributes = True


class BookCreate(BaseModel):
    title: str
    author: str
    genre: str
    description: str
    is_available: bool
    release_date: datetime


class BookDetailShow(BaseModel):
    id: int
    title: str
    author: str
    genre: str
    description: str
    is_available: bool
    release_date: datetime
    owner_id: int
    holder_id: int | None

    # makes pydantic converting non dict objects to json
    class Config:
        from_attributes = True
