from pydantic import BaseModel, validator

from app.schemas.book_schema import BookShow


class UserShow(BaseModel):
    id: int
    username: str
    is_active: bool
    owned_books: list[BookShow]
    held_books: list[BookShow]

    # makes pydantic converting non dict objects to json
    class Config:
        from_attributes = True


class UserCreate(BaseModel):
    username: str
    password: str

    @validator("password")
    def validate_password(cls, v):
        if not v:
            raise ValueError("Password field is required")
        if v and len(v) < 5:
            raise ValueError("Password must be longer than 5 characters")
        return v
