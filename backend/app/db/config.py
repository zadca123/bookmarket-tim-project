import os
from pathlib import Path

from dotenv import load_dotenv
from pydantic import BaseModel

env_path = Path("./.env")
load_dotenv(dotenv_path=env_path)


class Settings(BaseModel):
    """Settings for the database"""

    PROJECT_NAME: str = os.getenv("PROJECT_NAME", "No project name specified")
    PROJECT_VERSION: str = os.getenv("PROJECT_VERSION", "0.0.1")

    USE_SQLITE_DB: bool = bool(os.getenv("USE_SQLITE_DB", False))
    if USE_SQLITE_DB:
        print("Using SQLITE3 database")
        DATABASE_URL: str = "sqlite:///./sqlite_database.db"
    else:
        print("Using POSTGRESQL database")
        POSTGRES_USER: str = os.getenv("POSTGRES_USER", "postgres")
        POSTGRES_PASSWORD: str = os.getenv("POSTGRES_PASSWORD", "postgres")
        POSTGRES_SERVER: str = os.getenv("POSTGRES_SERVER", "database")
        POSTGRES_PORT: str = os.getenv("POSTGRES_PORT", "5432")
        POSTGRES_DB: str = os.getenv("POSTGRES_DB", "postgres")
        DATABASE_URL: str = (
            f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@"
            f"{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}"
        )

    SECRET_KEY: str = os.getenv("SECRET_KEY", "qwerty123")
    JWT_ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 360  # in mins
    TEST_USER_EMAIL: str = "test@example.com"


settings = Settings()
