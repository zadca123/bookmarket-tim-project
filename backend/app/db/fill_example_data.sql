delete from public.book;
delete from public.user;


-- PASSWORD IS 'password' FOR EACH USER
INSERT INTO public.user
(id, username, is_superuser, is_active, hashed_password)
VALUES(1, 'suser', true, true, '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8');
INSERT INTO public.user
(id, username, is_superuser, is_active, hashed_password)
VALUES(2, 'disabled-user', false, false, '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8');
INSERT INTO public.user
(id, username, is_superuser, is_active, hashed_password)
VALUES(3, 'jakub', false, true, '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8');
INSERT INTO public.user
(id, username, is_superuser, is_active, hashed_password)
VALUES(4, 'adam', false, true, '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8');
INSERT INTO public.user
(id, username, is_superuser, is_active, hashed_password)
VALUES(5, 'oskar', false, true, '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8');


INSERT INTO public.book
(id, title, author, genre, description, is_available, release_date, owner_id, holder_id)
VALUES(1, 'W pustyni i w puszczy', 'Henryk Sienkiewicz', 'literatura młodzieżowa', 'Tę jedną z najpoczytniejszych powieści Henryka Sienkiewicza, wydaną w nowej szacie graficznej, przygotowano z myślą nie tylko o uczniach szkoły podstawowej, dla których jest to lektura obowiązkowa, ale także o wszystkich czytelnikach zafascynowanych opowieściami o dalekich podróżach. Pełna przygód i zwrotów akcji historia dwójki dzieci – Stasia i Nel – i ich wędrówki po Czarnym Lądzie wciąż porusza serca i działa na wyobraźnię. ', true, '1911-10-16', 3, null);
INSERT INTO public.book
(id, title, author, genre, description, is_available, release_date, owner_id, holder_id)
VALUES(2, 'Chłopki. Opowieść o naszych babkach', 'Joanna Kuciel-Frydryszak', 'reportaż', 'Autorka Służących do wszystkiego wraca do tematu wiejskich kobiet, ale tym razem to opowieść zza drugiej strony drzwi chłopskiej chałupy. Podczas, gdy Maryśki i Kaśki wyruszają do miast, by usługiwać w pańskich domach, na wsiach zostają ich siostry i matki: harujące od świtu do nocy gospodynie, folwarczne wyrobnice, mamki, dziewki pracujące w bogatszych gospodarstwach. Marzące o własnym łóżku, butach, szkole i o zostaniu panią. Modlące się o posag, byle „nie wyjść za dziada” i nie zostać wydane za morgi. Dzielące na czworo zapałki, by wyżywić rodzinę. Często analfabetki, bo „babom szkoły nie potrzeba”. Nasze babki i prababki. Joanna Kuciel-Frydryszak daje wiejskim kobietom głos, by opowiedziały o swoim życiu: codziennym znoju, lękach i marzeniach. Ta mocna, głęboko dotykająca lektura pokazuje siłę kobiet, ich bezgraniczne oddanie rodzinie, ale też pragnienie zmiany i nierówną walkę o siebie w patriarchalnym społeczeństwie. ', false, '2023-05-17', 4, 3);
INSERT INTO public.book
(id, title, author, genre, description, is_available, release_date, owner_id, holder_id)
VALUES(3, 'Wartka śmierć', 'Robert Galbraith', 'kryminał', 'Do Cormorana Strike’a zgłasza się zaniepokojony ojciec, którego syn Will przyłączył się do sekty religijnej gdzieś w wiejskiej głuszy hrabstwa Norfolk. Powszechny Kościół Humanitarny to z pozoru pokojowa organizacja dążąca do zmiany świata na lepsze. Strike odkrywa jednak, że pod szlachetnymi frazesami kryją się nieczyste intencje i zagadkowe zgony. Aby ratować Willa, wspólniczka Cormorana Robin Ellacott postanawia przeniknąć do sekty i zamieszkać wśród jej członków. Nie jest jednak gotowa na okrucieństwa, których będzie świadkiem, ani na cenę, jaką przyjdzie jej zapłacić za swoje zawodowe ambicje. "Wartka śmierć" to siódmy tom epickiej serii kryminalnej – przepełniona niepokojem i wielkimi emocjami kontynuacja losów Robin i Strike’a. ', false, '2023-12-06', 3, 5);
INSERT INTO public.book
(id, title, author, genre, description, is_available, release_date, owner_id, holder_id)
VALUES(4, 'Dawno temu w Warszawie', 'Jakub Żulczyk', 'kryminał', 'Stało się. Świat się skończył. Nadeszło Groźne i Inne. Przestańcie się mazać, to dopiero początek Nie zapinajcie pasów. To bez sensu. 2014 Jacek Nitecki stoi samotny przed halą odlotów. Zaczyna padać deszcz. Dzwoni telefon. Jacek odbiera, myśląc, że jego droga do piekła właśnie się skończyła. Ale ona dopiero się zaczyna. 2020 Warszawa wchodzi w kolejną falę pandemii. Lockdown zamyka ludzi w domach, w ich głowach, kleszczach lęku. W środku tego szaleństwa Marta Pazińska próbuje walczyć o życie i godność tych, którzy są samotni i bezbronni. Nie rozumie jednak, że cała Warszawa stała się bezbronna jak dziecko. A ci, którzy przez lata czaili się w ciemności, teraz ruszyli po władzę. Idą po nią po trupach dzieciaków, policjantów, polityków. Kości chrzęszczą pod ich stopami. Parę lat po wydarzeniach od "Ślepnąc od świateł" bohaterowie tamtej powieści schodzą pod powierzchnię miasta, zanurzają się w płynącym pod nim złu. A zło jest zimne i gęste.', false, '2023-10-16', 5, 4);
INSERT INTO public.book
(id, title, author, genre, description, is_available, release_date, owner_id, holder_id)
VALUES(5, 'Polska na wojnie', 'Zbigniew Parafianowicz', 'reportaż', 'Co działo się w Pałacu Prezydenckim, w polskich służbach i w wojsku po wybuchu wojny na Ukrainie? Czy między Andrzejem Dudą a Wołodymyrem Żełenskim była prawdziwa przyjaźń? Jak małostkowe konflikty po kilku miesiącach wspólnej gry wdarły się do wielkiej polityki i co z tego wynikło? Nieznane fakty, kulisy rozmów z Ukraińcami, Amerykanami, Niemcami, pokazują i wielkość, i małość polskiej polityki.', false, '2023-11-08', 4, 5);
INSERT INTO public.book
(id, title, author, genre, description, is_available, release_date, owner_id, holder_id)
VALUES(6, 'Gdzie śpiewają raki', 'Delia Owens', 'literatura piękna', 'Światowa sensacja, bijący wszelkie rekordy fenomen, który przykuł uwagę ponad 13 milionów czytelników.', true, '2018-08-14', 4, null);