from sqlalchemy import Boolean, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models.base_model import Base


class User(Base):
    id: Mapped[int] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(String, unique=True)
    is_superuser: Mapped[bool] = mapped_column(Boolean, default=False)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)
    hashed_password: Mapped[str] = mapped_column(String)
    owned_books: Mapped[list["Book"]] = relationship(  # noqa
        back_populates="owner",
        foreign_keys="[Book.owner_id]",
        cascade="all, delete-orphan",
    )
    held_books: Mapped[list["Book"]] = relationship(  # noqa
        back_populates="holder",
        foreign_keys="[Book.holder_id]",
        cascade="all, delete-orphan",
    )
