from datetime import datetime

from sqlalchemy import Boolean, DateTime, ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.models.base_model import Base


class Book(Base):
    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str] = mapped_column(String)
    author: Mapped[str] = mapped_column(String)
    genre: Mapped[str] = mapped_column(String, nullable=True)
    description: Mapped[str] = mapped_column(String, nullable=True)
    is_available: Mapped[bool] = mapped_column(Boolean)
    release_date: Mapped[datetime] = mapped_column(DateTime)
    owner_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    owner: Mapped["User"] = relationship(  # noqa
        back_populates="owned_books", foreign_keys=[owner_id]
    )
    holder_id: Mapped[int] = mapped_column(ForeignKey("user.id"), nullable=True)
    holder: Mapped["User"] = relationship(  # noqa
        back_populates="held_books", foreign_keys=[holder_id]
    )

    def __str__(self) -> str:
        return self.title
