from fastapi import HTTPException
from sqlalchemy.orm import Session

from app.models.book_model import Book
from app.schemas.book_schema import BookCreate
from app.schemas.user_schema import UserShow


def get_all(db: Session) -> list[Book]:
    books = db.query(Book).all()
    return books


def create(book: BookCreate, user: UserShow, db: Session) -> Book:
    book: Book = Book(
        title=book.title,
        author=book.author,
        genre=book.genre,
        description=book.description,
        is_available=book.is_available,
        release_date=book.release_date,
        owner_id=user.id,
        owner=user,
        holder_id=None,
        holder=None,
    )
    db.add(book)
    db.commit()
    db.refresh(book)
    return book


def get_book_by_id(db: Session, book_id: int):
    book = db.query(Book).filter(Book.id == book_id).first()
    if book is None:
        raise HTTPException(status_code=404, detail="Book not found")
    return book


def delete_book_by_id(db: Session, book_id: int):
    book = get_book_by_id(db, book_id)
    db.delete(book)
    db.commit()


def update_book(db: Session, book_id: int, book_data: BookCreate):
    book = get_book_by_id(db, book_id)
    for key, value in book_data.dict().items():
        setattr(book, key, value)
    db.commit()
    db.refresh(book)
    return book


def borrow_or_return(book_id: int, user_id: int, db: Session):
    book = get_book_by_id(db, book_id)
    if isinstance(book.holder_id, int) and user_id != book.holder_id:
        raise HTTPException(status_code=401, detail="Not authorized")
    elif book.holder_id is None:
        book.holder_id = user_id
    elif book.holder_id == user_id:
        book.holder_id = None
    db.commit()
    db.refresh(book)
    return book
