from fastapi import HTTPException
from sqlalchemy.orm import Session

from app.models.user_model import User
from app.schemas.user_schema import UserCreate
from app.utils.password_hasher import PasswordHasher


def get_all(db: Session) -> list[User]:
    users = db.query(User).all()
    return users


def get_by_id(db: Session, user_id: int) -> User:
    user = db.query(User).filter(User.id == user_id).first()
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return user


def get_by_username(username: str, db: Session) -> User:
    user = db.query(User).filter(User.username == username).first()
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return user


def create(user: UserCreate, db: Session) -> User:
    user: User = User(
        username=user.username,
        hashed_password=PasswordHasher(user.password).hash(),
        is_active=True,
        is_superuser=False,
    )
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


def check_if_exists(username: str, db) -> bool:
    user = db.query(User).filter(User.username == username).first()
    return user


def delete_by_id(db: Session, user_id: int):
    user = get_by_id(db, user_id)
    db.delete(user)
    db.commit()


def delete_by_username(username: str, db: Session):
    user = get_by_username(username, db)
    db.delete(user)
    db.commit()


def update(db: Session, user_id: int, user: UserCreate):
    user = get_by_id(db, user_id)
    for key, value in user.dict().items():
        setattr(user, key, value)
    db.commit()
    db.refresh(user)
    return user
