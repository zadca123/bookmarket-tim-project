from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app.api.login_router import get_current_user_from_token
from app.db.session import get_db
from app.queries import book_query
from app.schemas.book_schema import BookCreate, BookDetailShow, BookShow

router = APIRouter()

db_dependency = Annotated[Session, Depends(get_db)]
user_dependency = Annotated[Session, Depends(get_current_user_from_token)]


@router.get("/", response_model=list[BookShow])
def get_all(db: db_dependency):
    books = book_query.get_all(db=db)
    return books


@router.post("/", response_model=BookShow, status_code=201)
def create(book: BookCreate, current_user: user_dependency, db: db_dependency):
    book = book_query.create(book=book, user=current_user, db=db)
    return book


@router.get("/{book_id}", response_model=BookDetailShow, status_code=200)
def get_book_by_id(book_id: int, db: db_dependency):
    return book_query.get_book_by_id(db, book_id)


@router.delete("/{book_id}", status_code=204)
def delete_book(book_id: int, db: db_dependency):
    book_query.delete_book_by_id(db, book_id)
    return {"message": "Book deleted successfully"}


@router.put("/{book_id}", response_model=BookShow)
def update_book_details(book_id: int, book_data: BookCreate, db: db_dependency):
    try:
        updated_book = book_query.update_book(db, book_id, book_data)
        return updated_book
    except HTTPException:
        raise HTTPException(status_code=404, detail="Book not found")


@router.post("/{book_id}/borrow/", response_model=BookShow, status_code=201)
def borrow_or_return(book_id: int, user: user_dependency, db: db_dependency):
    book = book_query.borrow_or_return(book_id=book_id, user_id=user.id, db=db)
    return book
