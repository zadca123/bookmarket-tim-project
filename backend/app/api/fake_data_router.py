from typing import Annotated

from faker import Faker
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.api import book_router, user_router
from app.db.session import get_db
from app.schemas.book_schema import BookCreate
from app.schemas.user_schema import UserCreate

router = APIRouter()
db_dependency = Annotated[Session, Depends(get_db)]
fake = Faker()


def get_fake_user() -> UserCreate:
    username: str = fake.user_name()
    password: str = username

    user = UserCreate(
        username=username,
        password=password,
    )
    return user


def get_fake_book() -> BookCreate:
    title: str = fake.word()
    author: str = fake.name()
    genre: str = fake.word()
    description: str = fake.text()
    is_available: bool = True
    release_date = fake.date_this_decade()

    book = BookCreate(
        title=title,
        author=author,
        genre=genre,
        description=description,
        is_available=is_available,
        release_date=release_date,
    )
    return book


@router.get("/generate-users")
def create_fake_users(db: db_dependency):
    result = []
    for _ in range(15):
        user = get_fake_user()
        user_router.create(user=user, db=db)
        result.append(user)
    return result


@router.get("/generate-books")
def create_fake_books(db: db_dependency):
    result = []
    for user in user_router.get_all(db=db):
        book = get_fake_book()
        book_router.create(book=book, current_user=user, db=db)
        result.append(book)
    return result
