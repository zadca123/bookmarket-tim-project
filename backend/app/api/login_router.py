from datetime import timedelta
from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from sqlalchemy.orm import Session

from app.db.config import settings
from app.db.session import get_db
from app.models.user_model import User
from app.queries import user_query
from app.schemas.token_schema import Token
from app.utils.password_hasher import PasswordHasher
from app.utils.security import create_access_token

router = APIRouter()
db_dependency = Annotated[Session, Depends(get_db)]


def authenticate_user(
    username: str, plain_password: str, db: db_dependency
) -> User | bool:
    user = user_query.get_by_username(username=username, db=db)
    print(user)
    if not PasswordHasher(plain_password).compare(user.hashed_password):
        return False
    return user


@router.post("/token", response_model=Token)
def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()], db: db_dependency
) -> dict:
    user = authenticate_user(form_data.username, form_data.password, db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer", "user_id": user.id}


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login/token")


def get_current_user_from_token(
    token: Annotated[str, Depends(oauth2_scheme)], db: db_dependency
) -> User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
    )
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[settings.JWT_ALGORITHM]
        )
        username: str = payload.get("sub")
        print("username/email extracted is ", username)
        if username is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    user = user_query.get_by_username(username=username, db=db)
    if user is None:
        raise credentials_exception
    return user
