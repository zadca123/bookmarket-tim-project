from fastapi import APIRouter

from app.api import book_router, fake_data_router, login_router, user_router

api_router = APIRouter()
api_router.include_router(
    fake_data_router.router, prefix="/fake-data", tags=["fake-data"]
)
api_router.include_router(user_router.router, prefix="/user", tags=["user"])
api_router.include_router(book_router.router, prefix="/book", tags=["book"])
api_router.include_router(login_router.router, prefix="/login", tags=["login"])
