from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.api.login_router import get_current_user_from_token
from app.db.session import get_db
from app.queries import user_query
from app.schemas.user_schema import UserCreate, UserShow

router = APIRouter()
db_dependency = Annotated[Session, Depends(get_db)]
user_dependency = Annotated[UserShow, Depends(get_current_user_from_token)]


@router.get("/", response_model=list[UserShow])
def get_all(db: db_dependency):
    users = user_query.get_all(db=db)
    return users


@router.post("/", response_model=UserShow, status_code=201)
def create(user: UserCreate, db: db_dependency):
    if user_query.check_if_exists(user.username, db=db):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="User with this username already exist",
        )
    user = user_query.create(user=user, db=db)
    return user


@router.delete("/{user_id}", status_code=204)
def delete_by_id(user_id: int, current_user: user_dependency, db: db_dependency):
    if user_id == current_user.id or current_user.is_superuser:
        user_query.delete_by_id(user_id=user_id, db=db)
        return {"msg": "Successfully deleted."}
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED, detail="You are not permitted!!!!"
    )


@router.delete("/self", status_code=204)
def delete_self(current_user: user_dependency, db: db_dependency):
    try:
        user_query.delete_by_username(current_user.username, db=db)
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="You are not permitted!!!!"
        )

    return {"msg": "Successfully deleted."}
