import hashlib


class PasswordHasher:
    def __init__(self, text):
        self.text = text

    def hash(self) -> str:
        encoded_text = self.text.encode("utf-8")
        return hashlib.sha256(encoded_text).hexdigest()

    def compare(self, hashed_text: str) -> bool:
        return self.hash() == hashed_text
