# Table of Contents

1.  [How to run it](#orgdc2eac7)
    1.  [With poetry](#orgadb14cb)
    2.  [With docker](#org736cb46)


<a id="orgdc2eac7"></a>

# How to run it

First, copy .env.template to .env and edit values if you must (for
development, leave them as they are)

    cp .env.template .env


<a id="orgadb14cb"></a>

## With poetry

    make
    make run


<a id="org736cb46"></a>

## With docker

    docker-compose up --build
