import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from uvicorn.config import LOGGING_CONFIG

from app.api.base_router import api_router
from app.db.session import engine
from app.models.base_model import Base


def configure_database():
    print("Creating tables...")
    Base.metadata.create_all(bind=engine)


def configure_routers(app: FastAPI) -> None:
    app.include_router(api_router)


def configure_middleware(app: FastAPI) -> None:
    origins = ["*"]
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


def start_application():
    app = FastAPI()
    configure_routers(app)
    configure_database()
    configure_middleware(app)
    return app


app = start_application()


def run():
    LOGGING_CONFIG["formatters"]["default"][
        "fmt"
    ] = "%(asctime)s [%(name)s] %(levelprefix)s %(message)s"
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True, log_level="debug")


if __name__ == "__main__":
    run()
