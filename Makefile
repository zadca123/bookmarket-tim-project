.PHONY: clean

all: run

run:
	docker-compose up --build

clean:
	docker-compose down --remove-orphans
	docker system prune --volumes --force
	find . -type d -name __pycache__ -exec rm -rf {} +
	find . -type d -name node_modules -exec rm -rf {} +
