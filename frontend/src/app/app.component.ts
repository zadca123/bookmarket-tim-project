import { Component } from '@angular/core';
import {AuthService} from "./service/auth.service";
import {ToastService} from "./service/toast.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bookmarket-frontend';

  constructor(public authService: AuthService, public toastService: ToastService, public sanitizer: DomSanitizer) {
  }
}
