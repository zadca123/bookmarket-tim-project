import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RootViewComponent} from "./view/root-view.component";
import {AuthViewComponent} from "./view/auth-view.component";
import {AccountViewComponent} from "./view/account-view.component";
import {isSignedInGuard} from "./service/auth.guard";
import {BookDetailsViewComponent} from "./view/book-details-view.component";

const routes: Routes = [
  {
    path: '',
    component: RootViewComponent
  },
  {
    path: 'zaloguj',
    component: AuthViewComponent
  },
  {
    path: 'moje-konto',
    component: AccountViewComponent,
    canActivate: [isSignedInGuard]
  },
  {
    path: 'ksiazka/:id',
    component: BookDetailsViewComponent
  },
  {
    path: 'ksiazka/:id/edytuj',
    component: BookDetailsViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
