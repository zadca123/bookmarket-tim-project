import {ActivatedRouteSnapshot, CanActivateFn, Router} from "@angular/router";
import {inject} from "@angular/core";
import {AuthService} from "./auth.service";

export const isSignedInGuard: CanActivateFn = (next: ActivatedRouteSnapshot) => {
  console.log(next);
  const authService = inject(AuthService);
  const router = inject(Router);
  return authService.isAuthenticated ? true : router.navigateByUrl(`/zaloguj`);
};
