import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, map, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private httpClient: HttpClient) {
    this._token = localStorage.getItem("access_token") || undefined
    const userId = localStorage.getItem("user_id");
    this._userId = userId ? parseInt(userId) : undefined
    this._username = localStorage.getItem("username") || undefined
  }

  private _token?: string;
  private _username?: string;
  private _userId?: number;
  public get isAuthenticated() { return  this._token !== undefined }
  public get username() { return this._username }
  public get token() { return this._token }
  public get userId() { return this._userId }

  public login(username: string, password: string) {
    let body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };
    return this.httpClient.post("login/token", body.toString(), options).pipe(
      catchError(err => throwError(err)),
      map((value: any) => {
        this._token = value.access_token;
        this._userId = value.user_id;
        this._username = username;
        localStorage.setItem("access_token", value.access_token)
        localStorage.setItem("user_id", value.user_id)
        localStorage.setItem("username", username)
        return value;
      })
    )
  }

  public logout() {
    this._token = undefined;
    this._userId = undefined;
    this._username = undefined;
    localStorage.removeItem("access_token");
    localStorage.removeItem("user_id");
    localStorage.removeItem("username");
  }


}
