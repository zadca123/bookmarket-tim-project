import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Book, BookDetails} from "../model/Book";

@Injectable({
  providedIn: 'root'
})
export class BookService {
  constructor(private httpClient: HttpClient) { }

  public getBooks() {
    return this.httpClient.get<Book[]>("book");
  }

  public getBook(id: number) {
    return this.httpClient.get<BookDetails>(`book/${id}`)
  }

  public borrowBook(id: number) {
    return this.httpClient.post(`book/${id}/borrow`, {})
  }

  public deleteBook(id: number) {
    return this.httpClient.delete(`book/${id}`)
  }

  public updateBook(book: BookDetails) {
    return this.httpClient.put(`book/${book.id}`, {...book})
  }

  public addBook(book: BookDetails) {
    return this.httpClient.post<BookDetails>(`book`, {...book})
  }

}
