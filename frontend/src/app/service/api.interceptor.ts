import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";


@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(request.url.startsWith('/assets')) {
      return next.handle(request);
    }
    const apiUrl = "http://localhost:8000"
    const token = this.authService.token;
    const apiReq = request.clone({
      url: `${apiUrl}/${request.url}`,
      headers: token ? request.headers.set('Authorization', `bearer ${token}`) : request.headers
    });
    return next.handle(apiReq);
  }
}

