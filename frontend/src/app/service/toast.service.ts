import {Injectable} from "@angular/core";
import {Book} from "../model/Book";

export interface ToastInfo {
  header: string;
  body: string;
  delay?: number;
}

@Injectable({ providedIn: 'root' })
export class ToastService {

  toasts: ToastInfo[] = [];

  show(header: string, body: string) {
    this.toasts.push({ header, body });
  }

  remove(toast: ToastInfo) {
    this.toasts = this.toasts.filter(t => t != toast);
  }

  bookToast(book: Book, type: "borrow"|"return"|"delete"|"update"|"add", isSuccess: boolean) {
    let action = 'wypożyczona'
    if(type === 'return') action = 'oddana'
    else if(type === 'delete') action = 'usunięta'
    else if(type === 'update') action = 'zaktualizowana'
    else if(type === 'add') action = 'dodana'
    if(isSuccess)
      this.show("Sukces", `Książka <i>${book.title}</i> została ${action} pomyślnie`)
    else
      this.show("Błąd", `Książka <i>${book.title}</i> nie została ${action}`)
  }
}
