import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {RootViewComponent} from "./view/root-view.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ApiInterceptor} from "./service/api.interceptor";
import {AuthViewComponent} from "./view/auth-view.component";
import {LoadingComponent} from "./component/loading.component";
import {ErrorComponent} from "./component/error.component";
import {ErrorInterceptor} from "./service/error.interceptor";
import {ReactiveFormsModule} from "@angular/forms";
import {AccountViewComponent} from "./view/account-view.component";
import {BookComponent} from "./component/book.component";
import {BookDetailsViewComponent} from "./view/book-details-view.component";
import {QRCodeModule} from "angularx-qrcode";

@NgModule({
  declarations: [
    AppComponent,
    RootViewComponent,
    AuthViewComponent,
    AccountViewComponent,
    BookDetailsViewComponent,
    LoadingComponent,
    ErrorComponent,
    BookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    QRCodeModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
