import {Component, Input} from "@angular/core";
import {Book} from "../model/Book";
import {AuthService} from "../service/auth.service";

@Component({
  selector: 'app-book',
  template: `
    <div class="card w-100" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">
          {{book!.title}}
        </h5>
        <h6 class="card-subtitle mb-2 text-body-secondary">
          {{book!.author}}
          <ng-container *ngIf="showBadge">
            <span class="badge text-bg-primary me-1" *ngIf="book!.owner_id === authService.userId">Moja książka</span>
            <span class="badge text-bg-success" *ngIf="!book!.holder_id">Dostępna</span>
            <span class="badge text-bg-primary" *ngIf="book!.holder_id === authService.userId">Wypożyczona przeze mnie</span>
            <span class="badge text-bg-danger" *ngIf="book!.holder_id && book!.holder_id !== authService.userId">Wypożyczona</span>
          </ng-container>
        </h6>
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: []
})
export class BookComponent {
  @Input({required: true}) book?: Book;
  @Input() showBadge = true;

  constructor(public authService: AuthService) {
  }
}
