import {Component} from "@angular/core";

@Component({
  selector: 'app-loading',
  template: `
    <div class="text-center mt-5">
      <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
        <span class="visually-hidden">Loading...</span>
      </div>
    </div>
  `,
  styles: []
})
export class LoadingComponent {

}
