import {Component, Input} from "@angular/core";

@Component({
  selector: 'app-error',
  template: `
    <div class="alert alert-danger" role="alert">
      <h4 class="alert-heading">Błąd</h4>
      <p>{{message || "Wystąpił problem, spróbuj ponownie później"}}</p>
      <ng-container *ngIf="error">
        <hr>
        <p class="mb-0">{{error}}</p>
      </ng-container>
    </div>
  `,
  styles: []
})
export class ErrorComponent {
  @Input() message = ""
  @Input() error = ""
}
