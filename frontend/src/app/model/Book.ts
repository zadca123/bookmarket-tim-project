
export interface Book {
  id: number,
  title: string,
  author: string,
  owner_id: number,
  holder_id?: number
}

export interface BookDetails extends Book {
  genre: string,
  description: string,
  release_date: string
}
