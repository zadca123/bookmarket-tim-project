import {Book} from "./Book";

export interface User {
  id: number,
  username: string,
  owned_books: Book[],
  held_books: Book[]
}
