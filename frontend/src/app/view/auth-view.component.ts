import {Component} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../service/auth.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth-view',
  template: `
    <div style="max-width: 600px" class="mx-auto mt-4">
      <app-error *ngIf="error" [message]="error"></app-error>
      <div class="card mt-2">
        <form class="card-body" [formGroup]="form">
          <h5 class="card-title">Logowanie</h5>
          <div class="mb-3">
            <label for="username" class="form-label">Nazwa użytkownika</label>
            <input type="text" class="form-control" id="username" formControlName="username">
            <div class="text-danger" style="font-size: 0.8rem" *ngIf="wasSubmitted && form.controls.username.errors">Pole wymagane</div>
          </div>
          <div class="mb-3">
            <label for="password" class="form-label">Hasło</label>
            <input type="password" class="form-control" id="password" formControlName="password">
            <div class="text-danger" style="font-size: 0.8rem" *ngIf="wasSubmitted && form.controls.password.errors">Pole wymagane</div>
          </div>
          <button class="btn btn-primary w-100" type="submit" (click)="submit()">Zaloguj</button>
        </form>
      </div>
    </div>
  `,
  styles: []
})
export class AuthViewComponent {

  form = new FormGroup({
    username: new FormControl<string>("", {nonNullable: true, validators: Validators.required}),
    password: new FormControl<string>("", {nonNullable: true, validators: Validators.required})
  })
  wasSubmitted = false;
  error = "";

  constructor(private auhService: AuthService, private router: Router) {
  }

  submit() {
    this.wasSubmitted = true;
    this.error = ""
    if(this.form.valid) {
      this.auhService.login(this.form.controls.username.value, this.form.controls.password.value).subscribe({
        next: _ => {
          console.log("AuthViewComponent next")
          this.router.navigate(["/moje-konto"])
        },
        error: (err: HttpErrorResponse) => {
          if(err.status === 404 || err.status === 401)
            this.error = "Login lub hasło są nieprawidłowe"
        }
      })
    }
  }
}
