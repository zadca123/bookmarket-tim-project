import {Component, OnInit, TemplateRef} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {BookService} from "../service/book.service";
import {BookDetails} from "../model/Book";
import {AuthService} from "../service/auth.service";
import {ToastService} from "../service/toast.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-book-details-view',
  templateUrl: './book-details-view.component.html',
  styles: []
})
export class BookDetailsViewComponent implements OnInit {

  form = new FormGroup({
    title: new FormControl<string>(""),
    author: new FormControl<string>(""),
    release_date: new FormControl<string>(""),
    genre: new FormControl<string>(""),
    description: new FormControl<string>("")
  })

  isLoading = true;
  isEdit = false;
  error = "";
  data?: BookDetails;

  constructor(private route: ActivatedRoute, private bookService: BookService, public authService: AuthService,
              private toast: ToastService, private modalService: NgbModal, private router: Router) {}

  ngOnInit(): void {
    const id  = this.route.snapshot.params['id']
    this.isEdit = this.route.snapshot.url[this.route.snapshot.url.length-1].path === "edytuj"
    this.bookService.getBook(id).subscribe({
      next: value => {
        this.data = value;
        this.form.patchValue(value)
        if(value.owner_id !== this.authService.userId) {
          this.isEdit = false;
        }
        this.isLoading = false;
      },
      error: (err: Error) => {
        this.error = err.message;
        this.isLoading = false;
      }
    })
  }

  getQrCodeData() {
    return JSON.stringify({
      origin: 'Bookmarket',
      action: 'borrow/return',
      id: this.data!.id
    })
  }

  borrowBook(type: "borrow"|"return") {
    this.bookService.borrowBook(this.data!.id).subscribe({
      next: _ => {
        this.toast.bookToast(this.data!, type, true);
        this.data = {...this.data!, holder_id: type === "borrow" ? this.authService.userId : undefined}
      },
      error: _ => {
        this.toast.bookToast(this.data!, type, false);
      }
    })
  }

  deleteBook() {
    this.modalService.dismissAll()
    this.bookService.deleteBook(this.data!.id).subscribe({
      next: _ => {
        this.toast.bookToast(this.data!, "delete", true);
        this.router.navigate(["/"])
      },
      error: _ => {
        this.toast.bookToast(this.data!, "delete", false);
      }
    })
  }

  updateBook() {
    const updatedBook: BookDetails = {
      ...this.data!,
      title: this.form.controls.title.value || '',
      author: this.form.controls.author.value || '',
      genre: this.form.controls.genre.value || '',
      release_date: this.form.controls.release_date.value || '',
      description: this.form.controls.description.value || ''
    }
    this.bookService.updateBook(updatedBook).subscribe({
      next: _ => {
        this.isEdit = false;
        this.data = updatedBook;
        this.toast.bookToast(this.data!, 'update', true)
      },
      error: _ => {
        this.toast.bookToast(this.data!, 'update', false)
    }
    })
  }

  openModal(content: TemplateRef<any>) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
  }

}
