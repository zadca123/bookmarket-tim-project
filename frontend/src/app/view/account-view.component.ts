import {Component, OnInit, TemplateRef} from "@angular/core";
import {AuthService} from "../service/auth.service";
import {Router} from "@angular/router";
import {Book} from "../model/Book";
import {UserService} from "../service/user.service";
import {User} from "../model/User";
import {BookService} from "../service/book.service";
import {ToastService} from "../service/toast.service";
import {FormControl, FormGroup} from "@angular/forms";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-account-view',
  // templateUrl: './account-view.component.html',
  templateUrl: './account-view.component.html',
  styles: []
})
export class AccountViewComponent implements OnInit {

  form = new FormGroup({
    title: new FormControl<string>(""),
    author: new FormControl<string>(""),
    release_date: new FormControl<string>(""),
    genre: new FormControl<string>(""),
    description: new FormControl<string>("")
  })

  isLoading = true;
  error = "";
  data?: User;

  constructor(public authService: AuthService, private userService: UserService, private modalService: NgbModal,
              private bookService: BookService, private router: Router, private toast: ToastService) {
  }

  ngOnInit(): void {
     this.userService.get().subscribe({
       next: value => {
         this.data = value.find(value => value.username===this.authService.username);
         this.isLoading = false;
       },
       error: (err: Error) => {
         this.error= err.message;
         this.isLoading = false;
       }
     })
  }

  logout() {
    this.authService.logout();
    this.router.navigate(["/"])
  }

  returnBook(book: Book) {
    this.bookService.borrowBook(book.id).subscribe({
      next: _ => {
        this.toast.bookToast(book, "return", true);
        this.data!.held_books = this.data!.held_books.filter(b => b.id !== book.id);
      },
      error: _ => {
        this.toast.bookToast(book, "return", false);
      }
    })
  }

  addBook() {
    let bookToAdd = {
      title: this.form.controls.title.value,
      author: this.form.controls.author.value,
      genre: this.form.controls.genre.value ,
      release_date: this.form.controls.release_date.value + "T00:00:00",
      description: this.form.controls.description.value,
      is_available: true
    }
    this.bookService.addBook(bookToAdd as any).subscribe({
      next: value => {
        this.toast.bookToast(value, "add", true);
        this.modalService.dismissAll();
        this.data?.owned_books.push(value);
      },
      error: _ => {
        this.toast.bookToast(bookToAdd as any, "add", false);
      }
    })
  }

  openModal(content: TemplateRef<any>) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
  }

}
