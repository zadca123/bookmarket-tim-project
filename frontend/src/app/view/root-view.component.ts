import {Component, OnInit} from "@angular/core";
import {Book} from "../model/Book";
import {BookService} from "../service/book.service";
import {ToastService} from "../service/toast.service";
import {AuthService} from "../service/auth.service";

@Component({
  selector: 'app-root-view',
  template: `
    <app-error *ngIf="error" [error]="error"></app-error>
    <app-loading *ngIf="isLoading"></app-loading>
    <div *ngIf="!isLoading && !error && !data">
      Brak danych
    </div>
    <div class="row g-2 mt-2" *ngIf="!isLoading && !error && data">
      <div class="col-sm-12 col-lg-4" *ngFor="let book of data">
        <app-book [book]="book">
          <a class="card-link" [routerLink]="['ksiazka', book.id]">Szczegóły</a>
          <a class="card-link" role="button" *ngIf="authService.isAuthenticated && !book!.holder_id && book!.owner_id !== authService.userId" (click)="borrowBook(book, 'borrow')">Wypożycz</a>
          <a class="card-link" role="button" *ngIf="book!.holder_id === authService.userId" (click)="borrowBook(book, 'return')">Oddaj</a>
        </app-book>
      </div>
    </div>
  `,
  styles: []
})
export class RootViewComponent implements OnInit {
  isLoading = true;
  error = "";
  data: Book[] = [];

  constructor(public authService: AuthService, private bookService: BookService, private toast: ToastService) {
  }

  ngOnInit(): void {
    this.bookService.getBooks().subscribe({
      next: value => {
        this.data = value;
        this.isLoading = false;
      },
      error: (err: Error) => {
        console.log(err)
        this.error= err.message;
        this.isLoading = false;
      }
    })
  }

  borrowBook(book: Book, type: "borrow"|"return") {
    this.bookService.borrowBook(book.id).subscribe({
      next: _ => {
        this.toast.bookToast(book, type, true);
        this.data = this.data!.map(b => b.id === book.id ? {...b, holder_id: type === 'borrow' ? this.authService.userId : undefined} : b)
      },
      error: _ => {
        this.toast.bookToast(book, type, false);
      }
    })
  }
}
